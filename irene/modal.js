var modal = document.getElementById("newsLetterModal");
var btn = document.getElementById("newsLetterButton");
var span = document.getElementsByClassName("close")[0];
var submit = document.getElementById("submit");
var input = document.getElementById("input")

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

// hide the modal after submitting
submit.onclick = function() {
    modal.style.display = "none";
}
